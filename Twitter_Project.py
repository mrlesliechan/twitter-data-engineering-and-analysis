import tweepy
import pandas as pd
import pyodbc

#........import tools above........................

# consumer_key = " "
# consumer_secret = " "
# access_token = " "
# access_token_secret = " "

consumer_key = input('Please enter consumer_key: ')
consumer_secret = input('Please enter consumer_secret: ')
access_token = input('Please enter access_token: ')
access_token_secret = input('Please enter access_token_secret: ')
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth, wait_on_rate_limit=True)
#.......set up finished above......................

def tweets_extractor(search_word_1, search_word_2, tweet_number):
    try:
        tweets = tweepy.Cursor(api.search, q = (search_word_1, search_word_2)).items(int(tweet_number))
        tweets_list = [[tweet.created_at, tweet.user.screen_name, tweet.text, tweet.favorite_count] for tweet in tweets]
        tweets_df = pd.DataFrame(tweets_list, columns = ['Created_At', 'Screen_Name', 'Contents', 'Favorite_Count'])
        print(tweets_df)
        print("{} Tweets info extraction SUCCEED!!".format(tweet_number))       
    except:
        print("Tweets info extraction FAILED!!")
    return tweets_df
    

def userinfo_extractor(search_name_profile):
    try:
        userinfo = api.get_user(search_name_profile)
        userinfo_list = [userinfo.screen_name, userinfo.id, userinfo.description, userinfo.location]
        userinfo_df = pd.DataFrame(userinfo_list, index = ['User_Name', 'Id', 'Profile_Discription', 'Location'])
        userinfo_df = userinfo_df.transpose()
        print(userinfo_df)
        print("{}'s profile info extraction SUCCEED!!".format(search_name_profile))      
    except:
        print("User profile info extraction FAILED!!")
    return userinfo_df


def followerinfo_extractor(search_name_followerinfo, followers_number):
    try:
        followers = tweepy.Cursor(api.followers, screen_name = search_name_followerinfo).items(int(followers_number)) 
        followers_list = [[follower.screen_name, follower.id, follower.location] for follower in followers]
        followers_df = pd.DataFrame(followers_list, columns = ['User_Name', 'Id', 'Location']) 
        print(followers_df)      
        print("We have extracted {} follower's info from {}!SUCCEED!".format(followers_number, search_name_followerinfo))        
    except:
        print("Follower's info extraction FAILED!!")
    return followers_df


def info_loader (tweets_df, userinfo_df, followers_df):
    conn = pyodbc.connect('Driver={SQL Server};'
                      'Server=;'
                      'Database=Interim Project (Twitter);'
                      'Trusted_Connection=yes;')
    cursor = conn.cursor()
    # cursor.execute('CREATE TABLE Tweets(Created_At datetime, Screen_Name nvarchar(200), Contents nvarchar(200), Favorite_Count int)')
    # cursor.execute('CREATE TABLE UserProfile(User_Name nvarchar(20), Id int, Profile_Discription nvarchar(200), Location nvarchar(50))')
    # cursor.execute('CREATE TABLE SocialNetwork_Info(User_Name nvarchar(20), Id nvarchar(50), Location nvarchar(50))')
    try:
        cursor.execute('DELETE FROM Tweets')
        for index,row in tweets_df.iterrows():
            cursor.execute('INSERT INTO Tweets([Created_At],[Screen_Name],[Contents],[Favorite_Count]) values (?,?,?,?)', 
                            row['Created_At'], 
                            row['Screen_Name'], 
                            row['Contents'],
                            row['Favorite_Count'])
        print("tweets info loading to db SUCCEED!!")
    except:
        print("tweets info loading to db FAILED!!")

    try:
        cursor.execute('DELETE FROM UserProfile')
        for index,row in userinfo_df.iterrows():           
            cursor.execute('INSERT INTO UserProfile([User_Name],[Id],[Profile_Discription],[Location]) values (?,?,?,?)', 
                            row['User_Name'], 
                            row['Id'], 
                            row['Profile_Discription'],
                            row['Location'])
        print("User info loading to db SUCCEED!!")
    except:
        print("user info loading to db FAILED!!")
        
    try:
        cursor.execute('DELETE FROM SocialNetwork_Info')
        for index,row in followers_df.iterrows():
            cursor.execute('INSERT INTO SocialNetwork_Info([User_Name],[Id],[Location]) values (?,?,?)', 
                            row['User_Name'], 
                            row['Id'], 
                            row['Location'])   
        print("Followers info loading to db SUCCEED!!") 
    except:
        print("Followers info loading to db FAILED!!")        

    conn.commit()


def main():
    search_word_1 = input("Plz enter the first word you want to check in Tweet: ")
    search_word_2 = input("Plz enter the second word you want to check in Tweet (If you only want to search 1 key word, press ENTER here): ")
    tweet_number = input("How many Tweet you want to check? (please enter a number. For example: 1 or 20): ")
    search_name_profile = input("Which user's profile you would like to check? (If you hope to check @JoeBiden, please enter JoeBiden here): ")
    search_name_followerinfo = input("Which user you would like to check his/her followers' info? (If you hope to check @JoeBiden, please enter JoeBiden here): ")
    followers_number = input(("How many {}'s followers you want to check? (please enter a number. For example: 1 or 20): ").format(search_name_followerinfo))
 
    tweets_df = tweets_extractor(search_word_1, search_word_2, tweet_number)
    userinfo_df = userinfo_extractor(search_name_profile)
    followers_df = followerinfo_extractor(search_name_followerinfo, followers_number)

    info_loader(tweets_df, userinfo_df, followers_df)


if __name__ == "__main__":
    main()



